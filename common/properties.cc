// $Id$

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/properties.h"

#include "config.h"
#include <libgnome/libgnome.h>

#include "common/file.h"

#include <stdexcept>

#include <gtkmm/label.h>
#include <gtkmm/adjustment.h>
#include <gtkmm/box.h>
#include <gtkmm/frame.h>
#include <gtkmm/table.h>
#include <gtkmm/scrolledwindow.h>
#include <libgnomeuimm/dialog.h>

#define PROGNUM ((settings::progbutton_col) * (settings::progbutton_row))

namespace RadioActive
{

Properties::Properties(StationRadio &radio_):
    radio (radio_),
    fldStep (*manage (new Gtk::Adjustment (0.1, 0.05, 0.5, 0.05))),
    fldProgbutton_col (*manage (new Gtk::Adjustment (3, 1, 5))),
    fldProgbutton_row (*manage (new Gtk::Adjustment (3, 1, 5))),
    chkMuteExit (_("Mute radio on exit"),   0)
{
    set_title (_("RadioActive preferences"));
    close_hides (true);

    apply.connect (SigC::slot (*this, &Properties::apply_page));
    show.connect  (SigC::slot (*this, &Properties::update));
    
    // ********************
    // Page 1: General
    Gtk::VBox *vbox = new Gtk::VBox ();

    // Device settings
    Gtk::Frame *frame = new Gtk::Frame (_("Device"));
    Gtk::Table *table = new Gtk::Table (2, 2);
    table->set_border_width (5);
    table->set_col_spacings (5);
    
    // Radio device
    fldDev.changed.connect (SigC::slot (*this, &PropertyBox::changed));   
    Gtk::Label *label = new Gtk::Label (_("Radio device:"));
    label->set_alignment (0, 0.5);
    
    table->attach (*manage (label), 0, 1, 0, 1, GTK_FILL);
    table->attach (fldDev, 1, 2, 0, 1);
    
    // Frequency skip
    fldStep.set_digits (2);
    fldStep.set_numeric (true);
    fldStep.signal_changed ().connect (SigC::slot (*this, &PropertyBox::changed));
    label = new Gtk::Label (_("Tuning step (MHz):"));
    label->set_alignment (0, 0.5);

    table->attach (*manage (label), 0, 1, 1, 2, GTK_FILL);
    table->attach (fldStep, 1, 2, 1, 2);
    
    frame->set_border_width (5);
    frame->add (*manage (table));
    vbox->add  (*manage (frame));

    // User interface
    fldProgbutton_col.signal_changed ().connect (SigC::slot (*this, &PropertyBox::changed));
    fldProgbutton_row.signal_changed ().connect (SigC::slot (*this, &PropertyBox::changed));
    frame = new Gtk::Frame (_("User interface"));
    table = new Gtk::Table (2, 3);
    table->set_border_width (5);
    table->set_col_spacings (5);

    label = new Gtk::Label (_("Number of buttons in a row:"));
    label->set_alignment (0, 0.5);
    table->attach (*manage (label), 0, 1, 0, 1);
    table->attach (fldProgbutton_col, 1, 2, 0, 1);

    label = new Gtk::Label (_("Number of rows:"));
    label->set_alignment (0, 0.5);
    table->attach (*manage (label), 0, 1, 1, 2);
    table->attach (fldProgbutton_row, 1, 2, 1, 2);

    label = new Gtk::Label (_("Applying requires restarting RadioActive"));
    table->attach (*manage (label), 0, 2, 2, 3);

    frame->set_border_width (5);
    frame->add (*manage (table));
    vbox->add  (*manage (frame));

    // Persistency
    chkMuteExit.signal_clicked ().connect (SigC::slot (*this, &PropertyBox::changed));
    frame = new Gtk::Frame(_("Miscellaneous"));
    Gtk::VBox* options_vbox = new Gtk::VBox;
    
    options_vbox->set_border_width (5);
    options_vbox->set_spacing (5);
    options_vbox->pack_start (chkMuteExit, false);
    frame->set_border_width (5);
    frame->add (*manage (options_vbox));
    vbox->add (*manage (frame));
    
    // End of Page 1
    vbox->show_all ();
    general_page = append_page (
	*manage (vbox), *manage (new Gtk::Label (_("General"))));

    // ********************
    // Page 2: Stations
    frame = new Gtk::Frame();
    vbox = new Gtk::VBox();
    table = new Gtk::Table(3, settings::progbutton_row * settings::progbutton_col);
    Gtk::ScrolledWindow* view = new Gtk::ScrolledWindow;
    
    frame->set_border_width(5);
    table->set_border_width(5);
    view->set_usize(0, 100);

    prog_val = new Gtk::Entry*[PROGNUM];
    prog_name = new Gtk::Entry*[PROGNUM];
    for (int i = 0; i < PROGNUM; i++)
    {
	gchar* int_to_str = g_strdup_printf ("%d", (i + 1));
	label = new Gtk::Label (int_to_str);
	g_free(int_to_str);
	prog_val[i] = new Gtk::Entry;
	prog_name[i] = new Gtk::Entry;
	prog_val[i]->signal_changed ().connect  (SigC::slot (*this, &PropertyBox::changed));
	prog_name[i]->signal_changed ().connect (SigC::slot (*this, &PropertyBox::changed));
	
	table->attach(*manage(label),        0, 1, i, i + 1);
	table->attach(*manage(prog_val[i]),  1, 2, i, i + 1);
	table->attach(*manage(prog_name[i]), 2, 3, i, i + 1);
    }
    
    table->set_col_spacing (1, 5);
    view->add_with_viewport (*manage (table));
    view->set_policy (GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
    frame->add (*manage (view));
    vbox->add (*manage (frame));
    vbox->show_all ();
    stations_page = append_page (
	*manage(vbox), *manage (new Gtk::Label(_("Stations"))));
}

void Properties::apply_page (int page)
{
//    SpeedButtonData new_stations (PROGNUM, StationData (0, ""));
    
    if (page == general_page)
    {
	settings::mute_exit = chkMuteExit.get_active();
	settings::tune_step = fldStep.get_value_as_float ();
	settings::progbutton_col = fldProgbutton_col.get_value_as_int ();
	settings::progbutton_row = fldProgbutton_row.get_value_as_int ();

	std::string dev_filename = fldDev.get_text ();
	if (dev_filename != radio.get_filename ())
	{
	    try
	    {
		radio.open_dev (dev_filename);
	    }
	    catch (std::runtime_error e) {
		Gnome::Dialog *error_dialog = Gnome::Dialogs::error (e.what ());
		error_dialog->run ();

		fldDev.set_text (radio.get_filename ());
	    }
	}
    }
    if (page == stations_page)
    {
	StationRadio::StationList new_stations (PROGNUM, StationRadio::StationEntry (0, ""));
	
	for (int i = 0; i < PROGNUM; i++)
	{
	    std::string station_name = prog_name[i]->get_text ();
	    double station_freq = atof (prog_val[i]->get_text ().c_str ());
	    new_stations[i] = StationRadio::StationEntry (station_freq, station_name);
	}
	radio.set_stations (new_stations);
    }
}

void Properties::update()
{
    fldDev.set_text (radio.get_filename ());
    fldStep.set_value (settings::tune_step);
    fldProgbutton_col.set_value (settings::progbutton_col);
    fldProgbutton_row.set_value (settings::progbutton_row);
    chkMuteExit.set_active (settings::mute_exit);
    
    const StationRadio::StationList &stations = radio.get_stations ();
    char *tmp = 0;
    
    for (unsigned int i = 0; i < stations.size (); i++)
    {
	tmp = g_strdup_printf ("%.1f", stations[i].first);
	prog_val[i]->set_text (tmp);
	g_free (tmp);
	
	prog_name[i]->set_text (stations[i].second);
    }

    set_modified (false);
}

} // namespace RadioActive
