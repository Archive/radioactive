// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_PROPERTIES_H
#define RADIOACTIVE_PROPERTIES_H

#include "common/driver.h"

#include "radioactive.h"

#include <libgnomeuimm/propertybox.h>
#include <gtkmm/entry.h>
#include <gtkmm/spinbutton.h>
#include <gtkmm/checkbutton.h>

namespace RadioActive
{
    class Properties: public Gnome::PropertyBox
    {
	StationRadio &radio;
	
	// Page 1: General settings
	int general_page;

	// The radio device
	Gtk::Entry      fldDev;
	Gtk::SpinButton fldStep;
	
	// User interface
	Gtk::SpinButton fldProgbutton_col;
	Gtk::SpinButton fldProgbutton_row;

	Gtk::CheckButton chkMuteExit;
	
	// Page 2: Stations
	int stations_page;
	
	Gtk::Entry **prog_val;
	Gtk::Entry **prog_name;
	
    public:
	Properties (StationRadio &radio);

    private:
	void apply_page (int page);
	void update ();

	void update_stations ();
    };
}

#endif /* !RADIOACTIVE_PROPERTIES_H */
