// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/mute-button.h"

#include "config.h"
#include <libgnome/libgnome.h>

namespace RadioActive
{

namespace Widgets
{

MuteButton::MuteButton (Radio &radio_):
    Gtk::ToggleButton (_("Mute")),
    radio (radio_),
    update_block (0)
{
    radio.mute_changed.connect (slot (*this, &MuteButton::mute_changed_cb));

    // Set initial state
    mute_changed_cb ();
}

void MuteButton::mute_changed_cb ()
{
    update_block++;
    set_active (radio.get_mute ());
    update_block--;
}

void MuteButton::clicked_impl ()
{
    if (update_block)
	return;

    radio.set_mute (get_active ());
}

} // namespace Widgets

} // namespace RadioActive
