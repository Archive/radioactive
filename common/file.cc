// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/file.h"
#include "radioactive.h"

#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <libgnome/libgnome.h>
#include <locale.h>

#define RADIOACTIVE_CONFIG_ROOT "RadioActive"

namespace RadioActive
{

namespace settings /* Keep this in sync with common/file.h */
{
    double tune_step = 0.1;
    int    last_button = 0;
    bool   console = false;
    bool   mute_exit = false;

    int progbutton_row = 3;
    int progbutton_col = 3;
}
    
namespace
{
    static void load_stations (StationRadio &radio);
    static void load_stations_old (StationRadio &radio);
    static void save_stations (const StationRadio &radio);
}

std::string load_device_filename ()
{
    
    gnome_config_push_prefix("/" RADIOACTIVE_CONFIG_ROOT "/");
    std::string device_filename = gnome_config_get_string ("general/device=" RADIOACTIVE_DEVFILE);
    gnome_config_pop_prefix ();

    return device_filename;
}
    
void load_config (StationRadio &radio)
{
    load_stations (radio);
    
    gnome_config_push_prefix("/" RADIOACTIVE_CONFIG_ROOT "/");

    radio.tune_station (gnome_config_get_int("general/last_button=0"));
    radio.set_freq (gnome_config_get_float("general/last=0"));
    radio.set_volume (gnome_config_get_int("general/volume=10"));
    radio.set_mute (gnome_config_get_bool("general/mute=false"));

    settings::tune_step = gnome_config_get_float ("general/tune_step=0.1");
    settings::mute_exit = gnome_config_get_bool("general/mute_exit=false");

    settings::progbutton_col = gnome_config_get_int ("wndMain/progbutton_col=3");
    settings::progbutton_row = gnome_config_get_int ("wndMain/progbutton_row=3");

    gnome_config_pop_prefix();
}

void save_config (const StationRadio &radio)
{
    gnome_config_push_prefix("/" RADIOACTIVE_CONFIG_ROOT "/");

    // Device state
    gnome_config_set_string ("general/device", radio.get_filename ().c_str());
    gnome_config_set_float  ("general/last",   radio.get_freq ());
    gnome_config_set_int    ("general/volume", radio.get_volume ());
    gnome_config_set_bool   ("general/mute",   radio.get_mute ());
    gnome_config_set_int    ("general/last_button", radio.get_station_num ());

    // UI settings
    gnome_config_set_float  ("general/tune_step", settings::tune_step);
    gnome_config_set_bool   ("general/mute_exit", settings::mute_exit);

    // Window settings
    gnome_config_set_int ("wndMain/progbutton_col", settings::progbutton_col);
    gnome_config_set_int ("wndMain/progbutton_row", settings::progbutton_row);

    gnome_config_pop_prefix();

    save_stations (radio);
    
    gnome_config_sync();
}

namespace
{

static void load_stations_old (StationRadio &radio)
{
    // Check for $HOME/.radioactive/speedtuner
    std::string rc_filename (getenv("HOME"));
    rc_filename += "/" + std::string (RADIOACTIVE_RCDIR) + "/speedtuner";
    if (stat (rc_filename.c_str (), 0))
	return;

    const int station_num = settings::progbutton_row * settings::progbutton_col;
    StationRadio::StationList stations (station_num, StationRadio::StationEntry (0, ""));
    
    std::ifstream rc_stream (rc_filename.c_str ());
    
    // This is needed for the decimal delimeter
    char *old_locale = g_strdup (setlocale (LC_NUMERIC, NULL));
    setlocale (LC_NUMERIC, "C");

    int i = 0;
    while (!rc_stream.eof () && i < station_num)
    {
	double      freq;
	std::string name;
	    
	rc_stream >> freq;
	getline (rc_stream, name);
	
	std::string::size_type chomp_start;
	if ((chomp_start = name.find_first_not_of(" \t"))
	    != std::string::npos)
	    name.erase(0, chomp_start);
	
	stations[i] = StationRadio::StationEntry (freq, name);
    }

    // Restore locale
    setlocale (LC_NUMERIC, old_locale);
    g_free (old_locale);

    radio.set_stations (stations);
}

static void load_stations (StationRadio &radio)
{
    int last_version = gnome_config_get_int ("/" RADIOACTIVE_CONFIG_ROOT
					     "/general/last_version=0");
    if (last_version < 1)
    {
	// Update station format
	load_stations_old (radio);
	save_stations (radio);
	
	gnome_config_set_int ("/" RADIOACTIVE_CONFIG_ROOT "/general/last_version", 1);
	gnome_config_sync ();
	return;
    }

    const int station_num = settings::progbutton_row * settings::progbutton_col;
    
    StationRadio::StationList stations (station_num, StationRadio::StationEntry (0, ""));

    gnome_config_push_prefix("/" RADIOACTIVE_CONFIG_ROOT "/Stations/");

    for (int i = 0; i < station_num; i++)
    {
	char *name_key = g_strdup_printf ("name_%d", i);
	char *freq_key = g_strdup_printf ("freq_%d", i);
	
	char   *name = gnome_config_get_string (name_key);
	double  freq = gnome_config_get_float  (freq_key);

	if (name)
	    stations[i] = StationRadio::StationEntry (freq, name);
	g_free (name);

	g_free (name_key);
	g_free (freq_key);
    }

    gnome_config_pop_prefix();    
    
    radio.set_stations (stations);
}

static void save_stations (const StationRadio &radio)
{
    char *name_str, *value_str;

    const StationRadio::StationList &stations = radio.get_stations ();

    int station_num;
    StationRadio::StationList::const_iterator i;
    
    gnome_config_push_prefix("/" RADIOACTIVE_CONFIG_ROOT "/Stations/");

    for (i = stations.begin (), station_num = 0;
	 i != stations.end ();
	 i++, station_num++)
    {
	name_str  = g_strdup_printf ("name_%d", station_num);
	value_str = g_strdup_printf ("freq_%d", station_num);
	
	gnome_config_set_string (name_str, i->second.c_str ());
	gnome_config_set_float  (value_str, i->first);

	g_free (name_str);
	g_free (value_str);
    }
    
    gnome_config_pop_prefix();
    gnome_config_sync();
}

} // anonymous namespace

} // namespace RadioActive
