// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/volume.h"

#include "radioactive.h"
#include <gtkmm/scale.h>

#include "common/mute-button.h"

namespace RadioActive
{

namespace Widgets
{

Volume::Volume (Radio &radio_):
    radio (radio_),
    vol_adj (RADIOACTIVE_VOL_MIN, RADIOACTIVE_VOL_MIN, RADIOACTIVE_VOL_MAX, 1, 3),
    update_block (0)
{
    Gtk::VScale* vol_scale = new Gtk::VScale (vol_adj);
    vol_scale->set_draw_value (false);
    vol_scale->set_digits (0);
    vol_scale->set_value_pos (Gtk::POS_BOTTOM);

    Gtk::Widget *mute_button = new Widgets::MuteButton (radio);
    
    pack_start (*manage (vol_scale));
    pack_start (*manage (mute_button), true, true, 2);

    vol_adj.signal_value_changed ().connect (slot (*this, &Volume::vol_adj_cb));
    radio.volume_changed.connect (slot (*this, &Volume::volume_changed_cb));

    // Set initial state
    volume_changed_cb ();
}

void Volume::volume_changed_cb ()
{
    int vol = radio.get_volume ();
    
    update_block++;
    vol_adj.set_value (RADIOACTIVE_VOL_MAX - vol);
    update_block--;
}

void Volume::vol_adj_cb ()
{
    if (update_block)
	return;

    int vol = RADIOACTIVE_VOL_MAX - int (vol_adj.get_value ());
    
    radio.set_mute (false);
    radio.set_volume (vol);
}

} // namespace Widgets

} // namespace RadioActive
