//$Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_DRIVER_H
#define RADIOACTIVE_DRIVER_H

#include "radioactive.h"
#include <vector>
#include <string>
#include <stdexcept>
#include <sigc++/object.h>
#include <sigc++/signal.h>
#include <linux/videodev.h>

namespace RadioActive
{
    class Radio: public SigC::Object
    {
	typedef double Frequency;
	
	int         fd;
	int         vol;
	bool        mute;
	double      freq;
	std::string filename;
	
    public:
	Radio (const std::string &devicefile) throw (std::runtime_error);
	
	void open_dev (const std::string &devicefile) throw (std::runtime_error);
	
	std::string get_filename () const { return filename; };
	
	double get_freq_min () const;
	double get_freq_max () const;
	
	double get_freq () const { return freq; };
	void   set_freq (double freq);
	SigC::Signal0<void> freq_changed;
	
	int  get_volume () const { return vol; };
	void set_volume (int vol);
	SigC::Signal0<void> volume_changed;
	
	bool get_mute () const { return mute; };
	void set_mute (bool mute);
	SigC::Signal0<void> mute_changed;
	
    private:
	double get_freq_fact () const;
	struct video_audio va;
	struct video_tuner tuner;
    };

    class StationRadio: public Radio
    {
    public:
	typedef std::pair<double, std::string> StationEntry;
	typedef std::vector<StationEntry>      StationList;

    private:
	StationList  stations;
	unsigned int station_num;
	bool         station_set;
	
    public:
	StationRadio (const std::string &devicefile);
	
	const StationList  & get_stations () const { return stations; };
	const StationEntry & get_station  (int station_num) const { return stations[station_num]; };
	void                 set_stations (const StationList &stations);
	void                 set_station  (int station_num, const StationEntry &station);

	SigC::Signal0<void> stations_changed;
	SigC::Signal2<void, int, const StationEntry&> station_changed;

	void tune_station (unsigned int station_num);
	unsigned int get_station_num   () const { return station_num; };
	bool         get_station_tuned () const { return station_set; };
	SigC::Signal2<void, bool, unsigned int> station_tuned;

    private:
	bool inside_tune_station;	
	void freq_changed_cb ();
    };
}
    
#endif /* !RADIOACTIVE_DRIVER_H */
