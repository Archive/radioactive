// $Id$

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/repeat-button.h"

#include <gtkmm/main.h>

namespace RadioActive
{

namespace Widgets
{

RepeatButton::RepeatButton (int time):
    button_mask (0),
    timeout_time (time)
{
}

RepeatButton::RepeatButton (const std::string &text,
			    int                time):
    Gtk::Button (text),
    button_mask (0),
    timeout_time (time)
{
}

bool RepeatButton::on_button_press_event (GdkEventButton *e)
{
    if (!button_mask && timeout_time)
    {
	hold.emit ();
	Glib::signal_timeout ().connect (SigC::slot (*this, &RepeatButton::timeout_handler),
					 timeout_time);
    }

    button_mask |= e->button;
    
    return false;
}

bool RepeatButton::on_button_release_event (GdkEventButton *e)
{
    button_mask &= !e->button;

    return false;
}

bool RepeatButton::timeout_handler ()
{
    if (!button_mask)
	return false;
    
    hold.emit ();
    return true;
}

} // namespace Widgets

} // namespace RadioActive
