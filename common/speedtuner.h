// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
 
#ifndef RADIOACTIVE_SPEEDTUNER_H
#define RADIOACTIVE_SPEEDTUNER_H

#include <gtkmm/table.h>
#include <gtkmm/togglebutton.h>
#include <gtkmm/tooltips.h>

#include "common/driver.h"

namespace RadioActive
{
    namespace Widgets
    {
	class SpeedTuner: public Gtk::Table
	{
	    StationRadio &radio;
	    
	    std::vector<Gtk::ToggleButton*> station_buttons;
	    Gtk::Tooltips tips;
	    
	public:
	    SpeedTuner (StationRadio &radio, int row, int col);
	    
	private:
	    bool update_block;
	    void prog_button_cb (int station_num);
	    void prog_button_mouse_cb (GdkEventButton *e, int button_num);
	    
	    void station_changed_cb (int station_num, const StationRadio::StationEntry &station);
	    void stations_changed_cb ();

	    void station_tuned_cb (bool tuned, unsigned int station_num);
	};
    }
}

#endif /* !RADIOACTIVE_SPEEDTUNER_H */
