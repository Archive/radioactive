// $Id$

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/wndMain.h"

#include "config.h"
#include <libgnome/libgnome.h>

#include <stdexcept>
#include <iostream>

#include "common/file.h"
#include "common/repeat-button.h"

#include "common/arrow_left.xpm"
#include "common/arrow_right.xpm"
#include "common/tune_left.xpm"
#include "common/tune_right.xpm"

#include "common/volume.h"
#include "common/tuner.h"
#include "common/speedtuner.h"
#if 0
#include "common/properties.h"
#endif

#include <sigc++/bind_return.h>

#include <libgnomeui/gnome-stock.h>

#include <gtkmm/table.h>
#include <gtkmm/box.h>
#include <gtkmm/image.h>
#include <gnomemm/app-helper.h>
#include <gnomemm/about.h>

namespace RadioActive
{

wndMain::wndMain (StationRadio &radio_):
    Gtk::Window (Gtk::WINDOW_TOPLEVEL),
    radio (radio_)
{
    using namespace SigC;
    
    set_title ("RadioActive " VERSION);
    set_border_width (5);
    set_policy (false, false, false);
    signal_button_press_event ().connect (SigC::bind_return (
	SigC::slot (*this, &wndMain::button_press_cb), false));

#if 0
    // Set window manager icon
#ifdef RADIOACTIVE_USE_WM_ICONS
    gnome_window_icon_set_from_file (gtkobj (), GNOME_ICONDIR "/gnome-radioactive.png");
#endif
#endif
    
    // Tuner
    Widgets::Tuner *tuner = new Widgets::Tuner (radio);
    
    // Tuner controls
    Widgets::RepeatButton* button = 0;
    Gnome::Pixmap* pixmap = 0;
    
    Gtk::Table* table = new Gtk::Table (3, 3);
    table->set_usize (180, 0);
    table->set_border_width (2);
    table->attach (*manage (tuner), 0, 3, 0, 1);
    
    // >
    pixmap = new Gnome::Pixmap (arrow_right_xpm);
    button = new Widgets::RepeatButton (RADIOACTIVE_TUNER_TIMEOUT_FINE);
    button->add (*manage (pixmap));
    button->hold.connect (SigC::bind (SigC::slot (*this, &wndMain::tuner_fine_tune), +1));
    table->attach (*manage (button), 2, 3, 1, 2);

    // <
    pixmap = new Gnome::Pixmap(arrow_left_xpm);
    button = new Widgets::RepeatButton (RADIOACTIVE_TUNER_TIMEOUT_FINE);
    button->add(*manage(pixmap));
    button->hold.connect (bind (slot (this, &wndMain::tuner_fine_tune), -1));
    table->attach(*manage(button), 1, 2, 1, 2);

    // >>
    pixmap = new Gnome::Pixmap (tune_right_xpm);
    button = new Widgets::RepeatButton (RADIOACTIVE_TUNER_TIMEOUT);
    button->add (*manage (pixmap));
    button->hold.connect (bind (slot (this, &wndMain::tuner_fine_tune), +5));
    table->attach (*manage (button), 2, 3, 2, 3);

    // <<
    pixmap = new Gnome::Pixmap(tune_left_xpm);
    button = new Widgets::RepeatButton (RADIOACTIVE_TUNER_TIMEOUT);
    button->add (*manage (pixmap));
    button->hold.connect (bind (slot (this, &wndMain::tuner_fine_tune), -5));
    table->attach (*manage (button), 1, 2, 2, 3);

    // Speedtuner
    Widgets::SpeedTuner *speed_tuner =
	new Widgets::SpeedTuner (radio, settings::progbutton_row, settings::progbutton_col);

    // Volume
    Widgets::Volume *volume = new Widgets::Volume (radio);

    // Key Events
    signal_key_press_event ().connect (SigC::bind_return (
	SigC::slot(*this, &wndMain::key_press_cb), false));
    
    // Main container
    Gtk::HBox* box = new Gtk::HBox (false, 5);
    box->pack_start (*manage (table));
    box->pack_start (*manage (speed_tuner), false, false);
    box->pack_start (*manage (volume), false, false);
    box->show_all ();
    add (*manage (box));
    
    set_events (GDK_BUTTON_PRESS_MASK|GDK_KEY_PRESS_MASK);

    // Popup menu
    {
	using namespace Gnome::UI;
	
	std::vector <Info> popup_menu_list;
	
	popup_menu_list.push_back (Item (Icon (GNOME_STOCK_MENU_ABOUT), _("_About"),
					 SigC::slot (&wndMain::about)));
	popup_menu_list.push_back (Item (Icon (GNOME_STOCK_MENU_PREF),_("_Preferences"),
					 SigC::slot (*this, &wndMain::show_properties)));
	
	Gnome::UI::fill (popup_menu, popup_menu_list, *(popup_menu.get_accel_group ()));
    }
}

void wndMain::button_press_cb (GdkEventButton* e)
{
    if (e->button == 3)
    {
	popup_menu.show_all ();
	popup_menu.popup (e->button, e->time);
    }
}

void wndMain::key_press_cb (GdkEventKey *k)
{
    switch (k->keyval)
    {
    case GDK_q:
	delete_event (0);
	break;
	
    case GDK_m:
	radio.set_mute (!radio.get_mute ());
	break;
	
    case GDK_1:
	radio.tune_station (0);
	break;
    case GDK_2:
	radio.tune_station (1);
	break;
    case GDK_3:
	radio.tune_station (2);
	break;
    case GDK_4:
	radio.tune_station (3);
	break;
    case GDK_5:
	radio.tune_station (4);
	break;
    case GDK_6:
	radio.tune_station (5);
	break;
    case GDK_7:
	radio.tune_station (6);
	break;
    case GDK_8:
	radio.tune_station (7);
	break;
    case GDK_9:
	radio.tune_station (8);
	break;
    case GDK_0:
	radio.tune_station (9);
	break;
    }
}

void wndMain::tuner_fine_tune (int weight)
{
    double delta = weight * settings::tune_step;
    radio.set_freq (radio.get_freq () + delta);
}

void wndMain::about ()
{
    std::string info = _("Radio tuner for Video4Linux-compatible devices");
    info += "\n";
    info += _("Visit http://cactus.rulez.org/projects/radioactive/ for more information");

    std::vector<std::string> authors;

    authors.push_back ("�RDI Gerg� <cactus@cactus.rulez.org>");
    
    Gnome::About* about = new Gnome::About("RadioActive", VERSION,
					   "(C) 1999-2003 �RDI Gerg�",
					   authors, info);
    about->run();
}

void wndMain::show_properties()
{
#if 0
    static RadioActive::Properties *prop_win = new RadioActive::Properties (radio);
    prop_win->run ();
#endif
}    

} // namespace RadioActive
