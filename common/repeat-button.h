// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_REPEAT_BUTTON_H
#define RADIOACTIVE_REPEAT_BUTTON_H

#include <gtkmm/button.h>

namespace RadioActive
{
    namespace Widgets
    {
	class RepeatButton: public Gtk::Button
	{
	public:
	    RepeatButton (int time = 0);
	    
	    RepeatButton (const std::string &text = "",
			  int                time = 0);
	    
	    SigC::Signal0<void> hold;
	private:
	    unsigned int button_mask;
	    int timeout_time;
	    bool on_button_press_event (GdkEventButton *e);
	    bool on_button_release_event (GdkEventButton *e);
	    bool timeout_handler ();
	};
    }
}

#endif /* !RADIOACTIVE_REPEAT_BUTTON_H */
