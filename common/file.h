// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_FILE_H
#define RADIOACTIVE_FILE_H

#include <vector>
#include <fstream>
#include <string>
#include "radioactive.h"

#include "common/driver.h"

namespace RadioActive
{
    namespace settings /* Keep this in sync with common/file.cc */
    {
	extern double tune_step;
	extern bool   console;
	extern bool   mute_exit;
	
	extern int progbutton_row, progbutton_col;
    }

    std::string load_device_filename ();    
    void load_config (StationRadio &radio);
    void save_config (const StationRadio &radio);
}

#endif // RADIOACTIVE_FILE_H
