// $Id$

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/tuner.h"
#include <math.h>

namespace RadioActive
{

namespace Widgets
{

Tuner::Tuner (Radio &radio_):
    radio (radio_),
    tuner_adj (radio.get_freq_min (),
	       radio.get_freq_min (),
	       radio.get_freq_max (), 0.5, 5),
    update_block (0)
{
    set_adjustment (tuner_adj);
    set_digits (2);

    tuner_adj.signal_value_changed ().connect (SigC::slot (*this, &Tuner::tuner_adj_cb));
    radio.freq_changed.connect (SigC::slot (*this, &Tuner::freq_changed_cb));

    // Set initial state
    freq_changed_cb ();
}

void Tuner::tuner_adj_cb ()
{
    if (update_block)
	return;
    
    double curr_value = tuner_adj.get_value ();
    double curr_value_int = rint (curr_value * 20);
    double rounded_value = curr_value_int / 20;

    if (rounded_value != curr_value)
	tuner_adj.set_value (rounded_value);
    else
	radio.set_freq (rounded_value);
}

void Tuner::freq_changed_cb ()
{
    update_block++;
    tuner_adj.set_value (radio.get_freq ());
    update_block--;
}

} // namespace Widgets

} // namespace RadioActive
