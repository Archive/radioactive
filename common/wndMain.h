// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_WNDMAIN_H
#define RADIOACTIVE_WNDMAIN_H

#include "common/driver.h"

#include <gtkmm/window.h>
#include <gtkmm/menu.h>

namespace RadioActive
{
    class wndMain: public Gtk::Window
    {
	StationRadio &radio;

	Gtk::Menu     popup_menu;
	
	int x, y;
	
    public:
	wndMain (StationRadio &radio);
	
	void show_properties ();
	static void about ();
	
    private:
	void tuner_fine_tune (int weigth);
	
	void button_press_cb (GdkEventButton *e);
	void key_press_cb    (GdkEventKey    *e);
	
	void dev_change (const std::string &dev);
    };
}
    
#endif /* !RADIOACTIVE_WNDMAIN_H */
