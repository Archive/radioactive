// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_VOLUME_H
#define RADIOACTIVE_VOLUME_H

#include <gtkmm/box.h>
#include <gtkmm/adjustment.h>

#include "common/driver.h"

namespace RadioActive
{
    namespace Widgets
    {
	class Volume: public Gtk::VBox
	{
	    Radio &radio;
	    
	    Gtk::Adjustment   vol_adj;
	    
	public:
	    Volume (Radio &radio);

	private:
	    int update_block;

	    void vol_adj_cb ();
	    void volume_changed_cb ();
	};
    }
}

#endif /* !RADIOACTIVE_VOLUME_H */
