// $Id$

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */
 
#include "common/speedtuner.h"

#include "config.h"
#include <libgnome/libgnome.h>

#include <sigc++/bind_return.h>

namespace RadioActive
{

namespace Widgets
{

SpeedTuner::SpeedTuner (StationRadio &radio_,
			int row, int col):
    Gtk::Table(row, col),
    radio (radio_),
    station_buttons (row * col),
    update_block (false)
{
    char* label;

    using namespace SigC;
    
    for (int i = 0; i < (row * col); i++)
    {
	label = g_strdup_printf("%d", (i + 1));
	Gtk::ToggleButton *b = new Gtk::ToggleButton (label);
	g_free (label);

	b->signal_clicked ().connect (SigC::bind (
	    SigC::slot (*this, &SpeedTuner::prog_button_cb), i));
	b->signal_button_press_event ().connect (SigC::bind_return (SigC::bind (
	    SigC::slot (*this, &SpeedTuner::prog_button_mouse_cb), i), true));

	b->set_size_request (30, 30);
	
	int left_attach = i % col;
	int top_attach  = i / col;
	
	attach (*manage (b),
		left_attach, left_attach + 1,
		top_attach, top_attach + 1,
		Gtk::FILL | Gtk::EXPAND, Gtk::FILL | Gtk::EXPAND,
		2, 2);
	station_buttons[i] = b;
    }

    radio.station_tuned.connect (SigC::slot (*this, &SpeedTuner::station_tuned_cb));
    radio.station_changed.connect (SigC::slot (*this, &SpeedTuner::station_changed_cb));
    radio.stations_changed.connect (SigC::slot (*this, &SpeedTuner::stations_changed_cb));
    
    // Update to initial state
    station_tuned_cb (radio.get_station_tuned (), radio.get_station_num ());
    stations_changed_cb ();
}

void SpeedTuner::prog_button_cb (int station_num)
{
    if (update_block)
	return;
    
    radio.tune_station (station_num);
}

void SpeedTuner::prog_button_mouse_cb (GdkEventButton *e, int button_num)
{
    if (e->button == 3)
    {
	gchar  *station_name = g_strdup_printf (_("Station %d"), button_num + 1);
	double  station_freq = radio.get_freq ();
	
	radio.set_station (button_num, StationRadio::StationEntry (station_freq, station_name));
	
	g_free (station_name);
    }
}

void SpeedTuner::station_changed_cb (int station_num, const StationRadio::StationEntry &station)
{
    char *tmp = g_strdup_printf ("%s (%0.1f)", station.second.c_str (), station.first);
    tips.set_tip (*station_buttons[station_num], tmp);
    g_free (tmp);
}
    
void SpeedTuner::stations_changed_cb ()
{
    const StationRadio::StationList &stations = radio.get_stations ();

    char *tmp;
    int j;
    StationRadio::StationList::const_iterator i;
    for (i = stations.begin(), j = 0;
	 i != stations.end(); i++, j++)
    {
	tmp = g_strdup_printf ("%s (%0.1f)", i->second.c_str (), i->first);
	tips.set_tip (*station_buttons[j], tmp);
	g_free (tmp);
    }
}

void SpeedTuner::station_tuned_cb (bool tuned, unsigned int station_num)
{
    update_block = true;
    
    // Un-toggle buttons
    for (std::vector<Gtk::ToggleButton*>::iterator i = station_buttons.begin ();
	 i != station_buttons.end (); i++)
    {
	(*i)->set_active (false);
    }

    if (!tuned)
    {
	update_block = false;
	return;
    }

    station_buttons[station_num]->set_active (true);
    update_block = false;
}
    

} // Widgets

} // RadioActive
