//$Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2003 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "common/driver.h"

#include "config.h"
#include <libgnome/libgnome.h>

#include <sigc++/object_slot.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <math.h>

namespace RadioActive
{

Radio::Radio (const std::string &devicefile) throw (std::runtime_error):
    vol (RADIOACTIVE_VOL_DEF)
{
    open_dev (devicefile);
}

void Radio::open_dev (const std::string &devicefile) throw (std::runtime_error)
{
    int fd_new;
    if (!((fd_new = open(devicefile.c_str(), O_RDONLY)) > 0))
    {
	char* errormsg_c = g_strdup_printf (_("Couldn't open radio device '%s'"),
					    devicefile.c_str ());
	std::string errormsg = errormsg_c;
	g_free (errormsg_c);
	throw std::runtime_error(errormsg);
    } else {
	if (fd)
	    close(fd);
	fd = fd_new;
    }

    tuner.tuner = 0;
    ioctl (fd, VIDIOCGTUNER, &tuner);

    filename = devicefile;
}

void Radio::set_volume (int vol_)
{
    vol = vol_;

    vol = std::min (vol, RADIOACTIVE_VOL_MAX);
    vol = std::max (vol, RADIOACTIVE_VOL_MIN);

    va.flags = VIDEO_AUDIO_VOLUME|(mute ? VIDEO_AUDIO_MUTE : 0);
    va.audio = 0;
    va.volume = vol * (65535 / (RADIOACTIVE_VOL_MAX - RADIOACTIVE_VOL_MIN));
    ioctl (fd, VIDIOCSAUDIO, &va);

    volume_changed ();
}

void Radio::set_mute (bool mute_)
{
    mute = mute_;

    if (mute)
    {
	va.volume = 0;
	va.flags = VIDEO_AUDIO_MUTE;
    } else {
	va.volume = vol * (65535 / (RADIOACTIVE_VOL_MAX - RADIOACTIVE_VOL_MIN));
	va.flags = VIDEO_AUDIO_VOLUME;
    }
    
    va.audio = 0;
    ioctl (fd, VIDIOCSAUDIO, &va);

    mute_changed ();
}

void Radio::set_freq  (double freq_)
{
    freq = freq_;

    struct video_tuner v;
    unsigned long xl_freq;
    
    xl_freq = (unsigned long)(freq * get_freq_fact () + 0.5);
    
    v.tuner = 0;
    
    ioctl (fd, VIDIOCSFREQ, &xl_freq);

    freq_changed ();
}

double Radio::get_freq_fact() const
{
    if ((tuner.flags & VIDEO_TUNER_LOW) == 0)
        return .016 * 1000;
    return 16 * 1000;
}

double Radio::get_freq_min() const
{
    return ((tuner.rangelow - 0.5) / get_freq_fact ());
}

double Radio::get_freq_max() const
{
    return ((tuner.rangehigh - 0.5) / get_freq_fact ());
}

StationRadio::StationRadio (const std::string &devicefile):
    Radio (devicefile),
    station_num (0),
    station_set (false),
    inside_tune_station (false)
{
    freq_changed.connect (SigC::slot (*this, &StationRadio::freq_changed_cb));
}

void StationRadio::set_stations (const StationRadio::StationList &stations_)
{
    stations = stations_;

    stations_changed ();
}

void StationRadio::set_station (int station_num, const StationRadio::StationEntry &station)
{
    stations[station_num] = station;

    if (station.first == get_freq ())
	station_tuned.emit (true, station_num);

    station_changed (station_num, station);
}

void StationRadio::tune_station (unsigned int station_num_)
{
    station_num = station_num_;
    station_set = true;
    
    inside_tune_station = true;

    set_freq (stations[station_num].first);
    station_tuned.emit (true, station_num);
    
    inside_tune_station = false;
}

void StationRadio::freq_changed_cb ()
{
    if (inside_tune_station)
	return;

    if (stations[station_num].first != get_freq ())
    {
	station_set = false;
	station_tuned.emit (false, 0);
    }
}

} // namespace RadioActive
