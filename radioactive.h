// RadioActive Copyright (C) 1999 �RDI Gerg� <cactus@cactus.rulez.org>
// 
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License version 2
// (included in the RadioActive distribution in doc/GPL) as published by
// the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

//$Id$ -*- c++ -*-
#ifndef RADIOACTIVE_H
#define RADIOACTIVE_H

#define RADIOACTIVE_RCDIR ".radioactive"
#define RADIOACTIVE_DEVFILE "/dev/radio0"

#define RADIOACTIVE_VOL_MIN 0
#define RADIOACTIVE_VOL_MAX 10
#define RADIOACTIVE_VOL_DEF 8

#define RADIOACTIVE_APPLET_TIMEOUT      1000 // Timeout for holding down the
                                             // mouse button on the applet buttons
#define RADIOACTIVE_TUNER_TIMEOUT       1000
#define RADIOACTIVE_TUNER_TIMEOUT_FINE  100

#endif /* !RADIOACTIVE_H */
