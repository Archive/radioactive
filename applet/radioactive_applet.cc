//$Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2002 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "config.h"
#include <libgnome/libgnome.h>

#include "applet/applet.h"

#include <iostream>

#include "common/file.h"
#include "common/driver.h"
#include "common/wndMain.h"

#include <panel--/applet-main.h>
#include <gnome--/dialog.h>
#include <stdexcept>

using namespace SigC;
using namespace Gnome;

int main(int argc, char** argv)
{
#ifdef ENABLE_NLS
    bindtextdomain(PACKAGE, GNOME_LOCALEDIR);
    textdomain(PACKAGE);
#endif
    
    Gnome::Applet_Main m ("radioactive_applet", VERSION, argc, argv);

    using namespace RadioActive;
    
    // Read device filename from configuration
    std::string dev_file = load_device_filename ();

    // Create driver instance
    StationRadio *radio;

    try {
	radio = new RadioActive::StationRadio (dev_file);
    }
    catch (std::runtime_error e)
    {
	std::cerr << e.what () << std::endl;
	
	Gnome::Dialog *error_dialog = Gnome::Dialogs::error (e.what ());
	error_dialog->run ();
	
	return 1;
    }
    
    // Restore settings
    load_config (*radio);

    // Create user interface
    RadioActive::Applet applet (*radio);
    applet.show();

    // Enter main loop
    m.run();
    
    // Save settings
    save_config (*radio);
    
    if (settings::mute_exit)
	radio->set_mute (true);
    
    return 0;
}
