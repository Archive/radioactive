// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2002 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "applet/applet.h"

#include "radioactive.h"
#include "config.h"

#include "common/repeat-button.h"
#include "common/file.h"
#include "common/wndMain.h"
#include "common/mute-button.h"
#include "common/properties.h"

#include <gtk--/table.h>
#include <gtk--/frame.h>

namespace RadioActive
{

Applet::Applet (StationRadio &radio_):
    Gnome::Applet ("radioactive_applet"),
    radio (radio_),
    station_label ("1"),
    button (0)
{
    using namespace SigC;

    // Set up driver callbacks
    radio.station_tuned.connect (slot (this, &Applet::station_tuned_cb));
    
    // < and > buttons
    Widgets::RepeatButton* btnPrev = new Widgets::RepeatButton ("<", RADIOACTIVE_APPLET_TIMEOUT);
    Widgets::RepeatButton* btnNext = new Widgets::RepeatButton (">", RADIOACTIVE_APPLET_TIMEOUT);

    set_widget_tooltip (*btnNext, _("Next station"));
    set_widget_tooltip (*btnPrev, _("Previous station"));

    btnPrev->hold.connect (slot (this, &Applet::prev_cb));
    btnNext->hold.connect (slot (this, &Applet::next_cb));

    Gtk::Table* table = new Gtk::Table (3, 2);
    table->set_col_spacings (5);
//    table.set_row_spacings(2);
    table->set_border_width(1);

#if 0
    station_label.set_justify (GTK_JUSTIFY_LEFT);
    station_label.set_alignment (0, 0.5);
#endif

    station_eventbox.add (station_label);

    Gtk::Widget *btnMute = new Widgets::MuteButton (radio);
    
    table->attach (station_eventbox, 1, 2, 0, 1);
    table->attach (*manage (btnPrev), 0, 1, 0, 1);
    table->attach (*manage (btnNext), 2, 3, 0, 1);
    table->attach (*manage (btnMute), 0, 3, 1, 2);
    
    // Create main applet frame
    Gtk::Frame* frame = new Gtk::Frame;
    frame->add(*manage(table));
    frame->show_all();

    add (*manage(frame));
    set_tooltip ("RadioActive " VERSION);

    // Set up right-click menu
    add_menuitem_stock ("preferences", GNOME_STOCK_MENU_PREF,
			_("Preferences..."),
			slot (this, &Applet::properties_cb));
    add_menuitem_stock ("window", GNOME_STOCK_MENU_VOLUME,
			_("Open window"),
			slot (this, &Applet::show_wnd_cb));
    add_menuitem_stock ("about", GNOME_STOCK_MENU_ABOUT,
			_("About"),
			slot (&wndMain::about));

    // Update state
    station_tuned_cb (radio.get_station_tuned (), radio.get_station_num ());
}

void Applet::station_tuned_cb (bool tuned, unsigned int station_num)
{
    if (!tuned)
    {
	station_label.set ("  ");

	char *tmp = g_strdup_printf ("(%.1f)", radio.get_freq ());
	set_widget_tooltip (station_eventbox, tmp);
	g_free (tmp);

	button = 0;
	
	return;
    }
    
    char *tmp;

    tmp = g_strdup_printf ("%d", station_num + 1);
    station_label.set (tmp);
    g_free (tmp);
    
    const StationRadio::StationEntry &station_data = radio.get_station (station_num);

    tmp = g_strdup_printf ("%s (%.1f)",
			   station_data.second.c_str (),
			   station_data.first);
    set_widget_tooltip (station_eventbox, tmp);
    g_free (tmp);

    button = station_num;
}

void Applet::next_cb ()
{
    const StationRadio::StationList &station_list = radio.get_stations ();

    if (++button > (settings::progbutton_col * settings::progbutton_row - 1))
	button = 0;
    
    static bool in_cycle = false;
    
    if (station_list[button].first == 0)
    {
	if (!in_cycle)
	{
	    in_cycle = true;
	    cycle_start = button;

	    next_cb ();
	    return;
	    
	} else {
	    
	    if (cycle_start != button)
	    {
		next_cb ();
		return;
	    }
	}
    }

    in_cycle = false;
    
    radio.tune_station (button);
}

void Applet::prev_cb ()
{
    const StationRadio::StationList &station_list = radio.get_stations ();
    
    if (--button < 0)
	button = (settings::progbutton_col * settings::progbutton_row) - 1;

    static bool in_cycle = false;

    if (station_list[button].first == 0)
    {
	if (!in_cycle)
	{
	    in_cycle = true;
	    cycle_start = button;

	    prev_cb ();
	    return;
	    
	} else {
	    
	    if (cycle_start != button)
	    {
		prev_cb ();
		return;
	    }
	}
    }

    in_cycle = false;

    radio.tune_station (button);
}

void Applet::show_wnd_cb ()
{
    static wndMain *w = new wndMain (radio);
    w->show();
}

void Applet::properties_cb ()
{
    static RadioActive::Properties *prop_win = new RadioActive::Properties (radio);
    prop_win->run ();
}    

} // namespace RadioActive
