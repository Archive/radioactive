// $Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2002 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef RADIOACTIVE_APPLET_H
#define RADIOACTIVE_APPLET_H

#include <panel--/applet.h>
#include <gtk--/label.h>
#include <gtk--/eventbox.h>

#include "common/driver.h"

namespace RadioActive
{
    class Applet: public Gnome::Applet
    {
	StationRadio &radio;

	Gtk::Label    station_label;
	Gtk::EventBox station_eventbox;

	int button;
	int cycle_start;
	
    public:
	Applet (StationRadio &radiodev);

    private:
	void show_wnd_cb ();
	void properties_cb ();
	
	void station_tuned_cb (bool tuned, unsigned int station_num);
	
	void next_cb ();
	void prev_cb ();
    };
}

#endif /* RADIOACTIVE_APPLET_H */
