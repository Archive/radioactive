//$Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2002 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <popt.h>

#include "config.h"
#include <libgnome/libgnome.h>

#include "cmdline.h"

#include "common/file.h"
#include "common/driver.h"

#include <stdexcept>
#include <iostream>
#include <gnome--/main.h>
#include <gnome--/dialog.h>

#include "common/wndMain.h"

using namespace SigC;
using namespace RadioActive;

static struct poptOption popt_cmdline[] =
{
    { 0, 0, POPT_ARG_INCLUDE_TABLE, get_popt_table (), 0,
      N_("RadioActive options:"), 0 },
    
    POPT_AUTOHELP
	
    { 0, 0, 0, 0, 0, 0, 0 }
};

static int stop (GdkEventAny *e)
{
    Gnome::Main::quit();
    return 0;
}

static RadioActive::StationRadio *create_radio ()
{
    using namespace RadioActive;
    
    // Read device filename from config file
    std::string dev_file = load_device_filename ();

    // Override config file value from command line
    if (cmd::dev_file != "")
	dev_file = cmd::dev_file;

    StationRadio *radio = new StationRadio (dev_file);
    
    // Restore settings
    load_config (*radio);
    
    // Apply command line settings
    apply_cmdline (*radio);
    
    return radio;
}

void run_cmdline (StationRadio &radio)
{
    // Turn on sound
    if (!cmd::just_mute)
	radio.set_mute (false);
    
    // Save settings
    save_config (radio);
}

int main_cmdline (int argc, char **argv)
{
    using namespace RadioActive;
    
    StationRadio *radio;
    
    // Parse command line
    poptContext popt_ctx = poptGetContext (0,
					   argc, (const char**)argv,
					   popt_cmdline,
					   0);
    while (poptGetNextOpt (popt_ctx) > 0);
    
    // Initialize driver
    try {
	radio = create_radio ();
    }
    catch (std::runtime_error e)
    {
	std::cerr << e.what () << std::endl;
	return 1;
    }

    // Do cmdline-specific stuff
    run_cmdline (*radio);
    
    // Finished
    return 0;
}

int main_gui (int argc, char **argv)
{
    using namespace RadioActive;
    
    Gnome::Main m (PACKAGE, VERSION,
		   argc, argv,
		   get_popt_table (), 0, 0);
    
    // Initialize driver
    StationRadio *radio;
    try {
	radio = create_radio ();
    }
    catch (std::runtime_error e) {
	std::cerr << e.what () << std::endl;
    
	if (!cmd::cmdline && !cmd::just_mute)
	{
	    Gnome::Dialog *error_dialog = Gnome::Dialogs::error (e.what ());
	    error_dialog->run ();
	}
	
	return 1;
    }
    
    // Do cmdline-specific stuff when neccessary
    if (cmd::cmdline || cmd::just_mute)
    {
	run_cmdline (*radio);
	return 0;
    }
    
    // Create main window
    wndMain w (*radio);
    w.delete_event.connect (slot (&stop));
    w.show ();

    // Enter main loop
    m.run ();

    // Save settings
    save_config (*radio);
    
    if (settings::mute_exit)
	radio->set_mute (true);

    return 0;
}

int main(int argc, char** argv)
{
#ifdef ENABLE_NLS
    bindtextdomain (PACKAGE, GNOME_LOCALEDIR);
    textdomain (PACKAGE);
#endif
    
    gnomelib_init (PACKAGE, VERSION);

    // Run command-line or GUI version, depending on $DISPLAY
    if (getenv ("DISPLAY"))
	return main_gui (argc, argv);
    else
	return main_cmdline (argc, argv);
}
