//$Id$ -*- c++ -*-

/* RadioActive Copyright (C) 1999-2002 �RDI Gerg� <cactus@cactus.rulez.org>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include "cmdline.h"

#include "config.h"
#include <libgnome/libgnome.h> // for I18N

#include <iostream>

namespace
{
    static void popt_parse_cb (poptContext ctx,
			       enum poptCallbackReason  reason,
			       const struct poptOption *opt,
			       const char *arg,
			       void       *closure);

    enum
    {
	CMD_FREQ    = -1,
	CMD_DEVFILE = -2,
	CMD_CMDLINE = -3,
	CMD_MUTE    = -4    
    };
    
    static struct poptOption popt_common[] =
    {
	{ 0, 0, POPT_ARG_CALLBACK, (void*)popt_parse_cb, 0 },
	
	{ "freq", 'f', POPT_ARG_DOUBLE, 0, CMD_FREQ,
	  N_("Set frequency"), N_("FREQ") },
	
	{ "device", 'd', POPT_ARG_STRING, 0, CMD_DEVFILE,
	  N_("Set Video4Linux device filename"), N_("DEVFILE") },
	
	{ "cmdline", 'c', POPT_ARG_NONE, 0, CMD_CMDLINE,
	  N_("Force command-line-only interface") },
	
	{ "mute", 'm', POPT_ARG_NONE, 0, CMD_MUTE,
	  N_("Mute the radio device and exit") },
	
	{ 0, 0, 0, 0, 0, 0, 0 }
    };
}

namespace
{
    
static void popt_parse_cb (poptContext ctx,
			   enum poptCallbackReason  reason,
			   const struct poptOption *opt,
			   const char *arg,
			   void       *closure)
{
    switch (opt->val)
    {
    case CMD_FREQ:
	RadioActive::cmd::freq = atof (arg);
	break;
    case CMD_DEVFILE:
	RadioActive::cmd::dev_file = arg;
	break;	
    case CMD_CMDLINE:
	RadioActive::cmd::cmdline = true;
	break;
    case CMD_MUTE:
	RadioActive::cmd::just_mute = true;
	break;
    }
}
    
} // Anonymous namespace

namespace RadioActive
{

namespace cmd
{
    double      freq      = 0;
    std::string dev_file  = "";
    bool        cmdline   = false;
    bool        just_mute = false;
}

poptOption *get_popt_table ()
{
    return popt_common;
}

void apply_cmdline (StationRadio &radio)
{
    if (cmd::freq != 0)
	radio.set_freq (cmd::freq);

    if (cmd::just_mute)
	radio.set_mute (true);
}
    
}
